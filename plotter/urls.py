from django.urls import path

from plotter.views.bot_view import BotView

urlpatterns = [
    path('', BotView.as_view(), name='index'),
    path('update/', BotView.as_view(), name='update'),
]