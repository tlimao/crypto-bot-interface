import json
from datetime import datetime
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.conf import settings

#from examples.bot_3 import BotMovingAverage
from examples.bot_2 import BotRsi
from context.crypto.data.backtest import BackTest
from context.crypto.infra.backtest_interface_impl import BackTestInterfaceImpl
from context.wallet.domain.entities.wallet import Wallet
from context.strategy.domain.entities.moving_average import MovingAverage
from context.strategy.domain.entities.rsi import Rsi

class BotView(TemplateView):
    TEMPLATE = "plotter/graph.html"

    repository = BackTestInterfaceImpl(
            BackTest(filename=settings.EXTERNAL_LIBS_PATH + "\\context\\crypto\\data\\btc.json"),
            datetime.strptime("2018-04-01", "%Y-%m-%d"),
            datetime.strptime("2021-01-24", "%Y-%m-%d"))

    wallet = Wallet("wallet", "Wallet Test", resources=1000.00)
    
    strategy = Rsi(period=14)

    bot = BotRsi(
        "id1",
        "Bot to RSI",
        repository,
        strategy,
        wallet)

    def get(self, request):
        if request.is_ajax():
            try:
                action, operation, currency, mm_value = self.bot.operate()

                wallet_resume = self.wallet.resume('BTC')

                data = {
                    'action': action,
                    'price': currency.last,
                    'timestamp': datetime.strftime(currency.timestamp, "%Y-%m-%d %H:%M:%S"),
                    'wallet': {
                        'qnt': wallet_resume.qnt,
                        'value': wallet_resume.mean_price,
                        'free': round(self.wallet.resources * 1.0, 2),
                        'now': round(wallet_resume.qnt * currency.last + self.wallet.resources, 2)
                    }
                }

                return HttpResponse(json.dumps(data))
            
            except Exception as e:
                print(e)
                return HttpResponse(None)
            
        else:
            context = {'currency': self.repository.get('BTC')}

            return render(request, self.TEMPLATE, context)